<?php

namespace Drupal\date_recur_adv\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\date_recur\DateRange;
use Drupal\date_recur\Entity\DateRecurInterpreterInterface;
use Drupal\date_recur\Plugin\Field\FieldFormatter\DateRecurBasicFormatter;
use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;

/**
 * Basic recurring date formatter.
 *
 * @FieldFormatter(
 *   id = "date_recur_advanced_formatter",
 *   label = @Translation("Date recur advanced formatter"),
 *   field_types = {
 *     "date_recur"
 *   }
 * )
 */
class DateRecurAdvancedFormatter extends DateRecurBasicFormatter {

  /**
   * {@inheritdoc}
   */
  protected function viewItem(DateRecurItem $item, $maxOccurrences): array {
    $cacheability = new CacheableMetadata();
    $build = [
      '#theme' => 'date_recur_advanced_formatter',
      '#is_recurring' => $item->isRecurring(),
    ];

    $startDate = $item->start_date;
    /** @var \Drupal\Core\Datetime\DrupalDateTime|null $endDate */
    $endDate = $item->end_date ?? $startDate;
    if (!$startDate || !$endDate) {
      return $build;
    }

    $build['#date'] = $this->buildDateRangeValue($startDate, $endDate, FALSE);

    // Render the rule.
    if ($item->isRecurring() && $this->getSetting('interpreter')) {
      /** @var string|null $interpreterId */
      $interpreterId = $this->getSetting('interpreter');
      if ($interpreterId && ($interpreter = $this->dateRecurInterpreterStorage->load($interpreterId))) {
        assert($interpreter instanceof DateRecurInterpreterInterface);
        $rules = $item->getHelper()->getRules();
        $plugin = $interpreter->getPlugin();
        $cacheability->addCacheableDependency($interpreter);
        $build['#interpretation'] = $plugin->interpret($rules, 'en');
      }
    }

    // Occurrences are generated even if the item is not recurring.
    $build['#occurrences'] = array_map(
      function (DateRange $occurrence): array {
        $startDate = DrupalDateTime::createFromDateTime($occurrence->getStart());
        $endDate = DrupalDateTime::createFromDateTime($occurrence->getEnd());
        return $this->buildDateRangeValue(
          $startDate,
          $endDate,
          TRUE
        );
      },
      $this->getOccurrences($item, $maxOccurrences)
    );

    $cacheability->applyTo($build);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildDateRangeValue(DrupalDateTime $startDate, DrupalDateTime $endDate, $isOccurrence): array {
    $this->formatType = $isOccurrence ? $this->getSetting('occurrence_format_type') : $this->getSetting('format_type');
    $startDateString = $this->buildDateWithIsoAttribute($startDate);

    // Show the range if start and end are different, otherwise only start date.
    if ($startDate->getTimestamp() === $endDate->getTimestamp()) {
      return $startDateString;
    }

    // Start date and end date are different.

    // Adjust format for single or multi day setting.
    $this->formatType = $startDate->format('Ymd') === $endDate->format('Ymd') ?
      $this->getSetting('same_end_date_format_type') :
      $this->getSetting('occurrence_format_type');

    // Check whether date range is same day and full day.
    if ($startDate->format('Ymd') === $endDate->format('Ymd')
        && $startDate->format('H:i') === '00:00'
        && $endDate->format('H:i') === '23:59') {
      $startDateString = $this->buildStartDateFullDayWithIsoAttribute($startDate);
      $endDateString = $this->buildEndDateFullDayWithIsoAttribute($endDate);
      $seperator = '';
    }

    // Check whether date range is same day and open-ended.
    elseif ($startDate->format('Ymd') === $endDate->format('Ymd')
        && $startDate->format('H:i') !== '00:00'
        && $endDate->format('H:i') === '23:59') {
      $startDateString = $this->buildStartDateOpenEndWithIsoAttribute($startDate);
      $endDateString = $this->buildEndDateOpenEndWithIsoAttribute($endDate);
      $seperator = '';
    }

    // Check whether date range is same day.
    elseif ($startDate->format('Ymd') === $endDate->format('Ymd')) {
      $startDateString = $this->buildStartDateSameDayWithIsoAttribute($startDate);
      $endDateString = $this->buildEndDateSameDayWithIsoAttribute($endDate);
      $seperator = $this->getSetting('separator');
    }

    // @todo Check whether date range is multiple years.
    elseif ($startDate->format('Ymd') !== $endDate->format('Ymd')
      && $startDate->format('H:i') === '00:00'
      && $endDate->format('H:i') === '23:59') {
      $startDateString = $this->buildStartDateMultiDayWithIsoAttribute($startDate);
      $endDateString = $this->buildEndDateMultiDayWithIsoAttribute($endDate);
      $seperator = $this->getSetting('separator');
    }

    // @todo Check whether date range is multiple months.
    elseif ($startDate->format('Ymd') !== $endDate->format('Ymd')
      && $startDate->format('H:i') === '00:00'
      && $endDate->format('H:i') === '23:59') {
      $startDateString = $this->buildStartDateMultiDayWithIsoAttribute($startDate);
      $endDateString = $this->buildEndDateMultiDayWithIsoAttribute($endDate);
      $seperator = $this->getSetting('separator');
    }

    // @todo Check whether date range is multiple years.
    elseif ($startDate->format('Ymd') !== $endDate->format('Ymd')
      && $startDate->format('H:i') === '00:00'
      && $endDate->format('H:i') === '23:59') {
      $startDateString = $this->buildStartDateMultiDayWithIsoAttribute($startDate);
      $endDateString = $this->buildEndDateMultiDayWithIsoAttribute($endDate);
      $seperator = $this->getSetting('separator');
    }

    // Check whether date range is multiple days.
    elseif ($startDate->format('Ymd') !== $endDate->format('Ymd')
        && $startDate->format('H:i') === '00:00'
        && $endDate->format('H:i') === '23:59') {
      $startDateString = $this->buildStartDateMultiDayWithIsoAttribute($startDate);
      $endDateString = $this->buildEndDateMultiDayWithIsoAttribute($endDate);
      $seperator = $this->getSetting('separator');
    }

    // This is the default formatting.
    else {
      $endDateString = $this->buildDateWithIsoAttribute($endDate);
      $seperator = $this->getSetting('separator');
    }

    return [
      'start_date' => $startDateString,
      'separator' => ['#plain_text' => $seperator],
      'end_date' => $endDateString,
    ];
  }

  /**
   * Creates a render array from a date object with ISO date attribute.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $date
   *   A date object.
   *
   * @return array
   *   A render array.
   */
  protected function buildStartDateFullDayWithIsoAttribute(DrupalDateTime $date) {
    // Create the ISO date in Universal Time.
    $iso_date = $date->format("Y-m-d\TH:i:s") . 'Z';
    $this->setTimeZone($date);
    return [
      '#theme' => 'time',
      '#text' => explode(' - ', $this->formatDate($date))[0],
      '#attributes' => [
        'datetime' => $iso_date,
      ],
      '#cache' => [
        'contexts' => [
          'timezone',
        ],
      ],
    ];
  }

  /**
   * Creates a render array from a date object with ISO date attribute.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $date
   *   A date object.
   *
   * @return array
   *   A render array.
   */
  protected function buildEndDateFullDayWithIsoAttribute(DrupalDateTime $date) {
    // Create the ISO date in Universal Time.
    $iso_date = $date->format("Y-m-d\TH:i:s") . 'Z';
    $this->setTimeZone($date);
    return [
      '#theme' => 'time',
      '#text' => $this->t('(full day)'),
      '#attributes' => [
        'datetime' => $iso_date,
      ],
      '#cache' => [
        'contexts' => [
          'timezone',
        ],
      ],
    ];
  }

  /**
   * Creates a render array from a date object with ISO date attribute.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $date
   *   A date object.
   *
   * @return array
   *   A render array.
   */
  protected function buildStartDateOpenEndWithIsoAttribute(DrupalDateTime $date) {
    // Create the ISO date in Universal Time.
    $iso_date = $date->format("Y-m-d\TH:i:s") . 'Z';
    $this->setTimeZone($date);
    return [
      '#theme' => 'time',
      '#text' => $this->formatDate($date),
      '#attributes' => [
        'datetime' => $iso_date,
      ],
      '#cache' => [
        'contexts' => [
          'timezone',
        ],
      ],
    ];
  }

  /**
   * Creates a render array from a date object with ISO date attribute.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $date
   *   A date object.
   *
   * @return array
   *   A render array.
   */
  protected function buildEndDateOpenEndWithIsoAttribute(DrupalDateTime $date) {
    // Create the ISO date in Universal Time.
    $iso_date = $date->format("Y-m-d\TH:i:s") . 'Z';
    $this->setTimeZone($date);
    return [
      '#theme' => 'time',
      '#text' => $this->t('(open end)'),
      '#attributes' => [
        'datetime' => $iso_date,
      ],
      '#cache' => [
        'contexts' => [
          'timezone',
        ],
      ],
    ];
  }

  /**
   * Creates a render array from a date object with ISO date attribute.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $date
   *   A date object.
   *
   * @return array
   *   A render array.
   */
  protected function buildStartDateSameDayWithIsoAttribute(DrupalDateTime $date) {
    // Create the ISO date in Universal Time.
    $iso_date = $date->format("Y-m-d\TH:i:s") . 'Z';
    $this->setTimeZone($date);
    return [
      '#theme' => 'time',
      '#text' => $this->formatDate($date),
      '#attributes' => [
        'datetime' => $iso_date,
      ],
      '#cache' => [
        'contexts' => [
          'timezone',
        ],
      ],
    ];
  }

  /**
   * Creates a render array from a date object with ISO date attribute.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $date
   *   A date object.
   *
   * @return array
   *   A render array.
   */
  protected function buildEndDateSameDayWithIsoAttribute(DrupalDateTime $date) {
    // Create the ISO date in Universal Time.
    $iso_date = $date->format("Y-m-d\TH:i:s") . 'Z';
    $this->setTimeZone($date);
    return [
      '#theme' => 'time',
      '#text' => explode(' - ', $this->formatDate($date))[1],
      '#attributes' => [
        'datetime' => $iso_date,
      ],
      '#cache' => [
        'contexts' => [
          'timezone',
        ],
      ],
    ];
  }

  /**
   * Creates a render array from a date object with ISO date attribute.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $date
   *   A date object.
   *
   * @return array
   *   A render array.
   */
  protected function buildStartDateMultiDayWithIsoAttribute(DrupalDateTime $date) {
    // Create the ISO date in Universal Time.
    $iso_date = $date->format("Y-m-d\TH:i:s") . 'Z';
    $this->setTimeZone($date);
    return [
      '#theme' => 'time',
      '#text' => explode(' - ', $this->formatDate($date))[0],
      '#attributes' => [
        'datetime' => $iso_date,
      ],
      '#cache' => [
        'contexts' => [
          'timezone',
        ],
      ],
    ];
  }

  /**
   * Creates a render array from a date object with ISO date attribute.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $date
   *   A date object.
   *
   * @return array
   *   A render array.
   */
  protected function buildEndDateMultiDayWithIsoAttribute(DrupalDateTime $date) {
    // Create the ISO date in Universal Time.
    $iso_date = $date->format("Y-m-d\TH:i:s") . 'Z';
    $this->setTimeZone($date);
    return [
      '#theme' => 'time',
      '#text' => explode(' - ', $this->formatDate($date))[0],
      '#attributes' => [
        'datetime' => $iso_date,
      ],
      '#cache' => [
        'contexts' => [
          'timezone',
        ],
      ],
    ];
  }

}
